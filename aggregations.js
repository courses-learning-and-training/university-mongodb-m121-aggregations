var pipeline1 = [
    {
        "$match": {$and: [
           {   "imdb.rating": { "$gte": 7 }},
           {"$and": [
            {"genres":{ $ne : "Crime"}},
            {"genres":{ $ne : "Horror"}}
         ]},
         {
            "$or": [
                {"rated": "PG"},
                {"rated": "G"},
             ]
         },
         {
            "$and": [
                { "languages": "English" },
                { "languages": "Japanese" },
            ]
         }
        ]} 
    },
    {
        $project: {            
            title:1,
            rated:1
        }
    }
]

chap2_lab1 = [
	{
		"$match" : {
			"cast" : {
				"$in" : [
					"Sandra Bullock",
					"Tom Hanks",
					"Julia Roberts",
					"Kevin Spacey",
					"George Clooney"
				]
			},
			"countries" : "USA",
			"tomatoes.viewer.rating" : {
				"$gte" : 3
			}
		}
	},
	{
		"$project" : {
			"title" : 1,
			"cast" : 1,
			"rate" : "$tomatoes.viewer.rating",
			"num_favs" : {
				"$size" : {
					"$filter" : {
						"input" : "$cast",
						"as" : "itm",
						"cond" : {
							"$in" : [
								"$$itm",
								[
									"Sandra Bullock",
									"Tom Hanks",
									"Julia Roberts",
									"Kevin Spacey",
									"George Clooney"
								]
							]
						}
					}
				}
			}
		}
	},
	{
		"$sort" : {
			"num_favs" : -1,
			"rate" : -1,
			"title" : -1
		}
	},
	{
		"$skip" : 24
	},
	{
		"$limit" : 1
	}
]

chap2_lab2 = [
	{
		"$match" : {
			"imdb.votes" : {
				"$gte" : 1
			},
			"imdb.rating" : {
				"$gte" : 1
			},
			"released" : {
				"$gte" : ISODate("1990-01-01T00:00:00Z")
			}
		}
	},
	{
		"$project" : {
			"_id" : 0,
			"rate" : "$imdb.rating",
			"title" : 1,
			"scaled_votes" : {
				"$add" : [
					1,
					{
						"$multiply" : [
							9,
							{
								"$divide" : [
									{
										"$subtract" : [
											"$imdb.votes",
											5
										]
									},
									{
										"$subtract" : [
											1521105,
											5
										]
									}
								]
							}
						]
					}
				]
			},
			"normalized_rating" : {
				"$avg" : [
					"$imdb.rating",
					"$scaled_votes"
				]
			}
		}
	},
	{
		"$sort" : {
			"normalized_rating" : 1
		}
	},
	{
		"$limit" : 1
	}
]

chap3_lab1 = [
	{
		"$match" : {
			"awards" : /won [0-9]* oscar/gi
		}
	},
	{
		"$project" : {
			"awards" : 1,
			"_id" : null,
			"highest" : {
				"$max" : "$imdb.rating"
			},
			"lowest" : {
				"$min" : "$imdb.rating"
			},
			"avg" : {
				"$avg" : "$imdb.rating"
			}
		}
	}
]

chap3_lab2 = [
	{
		"$match" : {
			"languages" : "English"
		}
	},
	{
		"$unwind" : "$cast"
	},
	{
		"$group" : {
			"_id" : "$cast",
			"title" : {
				"$push" : "$title"
			},
			"average" : {
				"$avg" : "$imdb.rating"
			}
		}
	},
	{
		"$project" : {
			"_id" : 1,
			"numFilms" : {
				"$size" : "$title"
			},
			"average" : "$average"
		}
	},
	{
		"$sort" : {
			"numFilms" : -1
		}
	},
	{
		"$limit" : 1
	}
]

chap3_lab3 = [
	{
		"$lookup" : {
			"from" : "air_routes",
			"localField" : "airlines",
			"foreignField" : "airline.name",
			"as" : "routes"
		}
	},
	{
		"$unwind" : "$routes"
	},
	{
		"$match" : {
			"routes.airplane" : {
				"$in" : [
					"747",
					"380"
				]
			}
		}
	},
	{
		"$group" : {
			"_id" : "$name",
			"airplane" : {
				"$push" : "$routes.airplane"
			}
		}
	},
	{
		"$project" : {
			"name" : "$_id",
			"no" : {
				"$size" : "$airplane"
			}
		}
	}
]

chap3_lab4 = 'db.air_alliances.aggregate([{   $match: { name: "OneWorld" } }, {   $graphLookup: {     startWith: "$airlines",     from: "air_airlines",     connectFromField: "name",     connectToField: "name",     as: "airlines",     maxDepth: 0,     restrictSearchWithMatch: {       country: { $in: ["Germany", "Spain", "Canada"] }     }   } }, {   $graphLookup: {     startWith: "$airlines.base",     from: "air_routes",     connectFromField: "dst_airport",     connectToField: "src_airport",     as: "connections",     maxDepth: 1   } }, {   $project: {     validAirlines: "$airlines.name",     "connections.dst_airport": 1,     "connections.airline.name": 1   } }, { $unwind: "$connections" }, {   $project: {     isValid: { $in: ["$connections.airline.name", "$validAirlines"] },     "connections.dst_airport": 1   } }, { $match: { isValid: true } }, { $group: { _id: "$connections.dst_airport" } } ]).itcount()'

chap4_lab1 = [
	{
		"$match" : {
			"imdb.rating" : {
				"$gt" : 0
			},
			"metacritic" : {
				"$gt" : 0
			}
		}
	},
	{
		"$facet" : {
			"imdbbucket" : [
				{
					"$sort" : {
						"imdb.rating" : -1
					}
				},
				{
					"$limit" : 10
				},
				{
					"$group" : {
						"_id" : "imdb",
						"title" : {
							"$push" : "$title"
						}
					}
				}
			],
			"metacriticbucket" : [
				{
					"$sort" : {
						"metacritic" : -1
					}
				},
				{
					"$limit" : 10
				},
				{
					"$group" : {
						"_id" : "metacritic",
						"title" : {
							"$push" : "$title"
						}
					}
				}
			]
		}
	},
	{
		"$project" : {
			"im" : "$imdbbucket.title",
			"meta" : "$metacriticbucket.title",
			"inter" : {
				"$setIntersection" : [
					"$imdbbucket,title",
					"$metacriticbucket.title"
				]
			}
		}
	}
]
